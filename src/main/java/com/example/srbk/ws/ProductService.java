package com.example.srbk.ws;

import com.example.srbk.model.Product;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@WebService
public interface ProductService {
    @WebMethod
    public List<Product> fetchAllProducts();

    @WebMethod
    public List<Product> fetchAllProductsUpdatedAfterDate(@WebParam Date updateTime);

    @WebMethod
    public void saveProduct(@WebParam Product product);
}
