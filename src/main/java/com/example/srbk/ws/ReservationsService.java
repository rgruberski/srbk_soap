package com.example.srbk.ws;

import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.exception.ReservationExpiredException;
import com.example.srbk.exception.ReservationNotExistsException;
import com.example.srbk.model.Reservation;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by robert on 15.01.15.
 */
@WebService
public interface ReservationsService {
    @WebMethod
    public Reservation getReservation(@WebParam Integer id) throws ReservationNotExistsException, ReservationExpiredException;

    @WebMethod
    public Reservation addPreliminaryReservation(@WebParam Reservation reservation) throws ReservationBusySeatException;

    @WebMethod
    public Reservation saveReservation(@WebParam Reservation reservation);
}
