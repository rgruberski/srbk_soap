package com.example.srbk.ws;

import com.example.srbk.model.Hall;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@WebService
public interface HallService {
    @WebMethod
    public List<Hall> fetchAllHalls();

    @WebMethod
    public List<Hall> fetchAllHallsUpdatedAfterDate(@WebParam Date updateTime);
}
