package com.example.srbk.ws;

import com.example.srbk.model.ProductCategory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 13.01.15.
 */
@WebService
public interface ProductCategoryService {
    @WebMethod
    public List<ProductCategory> fetchAllProductCategories();

    @WebMethod
    public List<ProductCategory> fetchAllProductCategoriesUpdatedAfterDate(@WebParam Date updateTime);
}
