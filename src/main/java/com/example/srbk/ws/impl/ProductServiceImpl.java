package com.example.srbk.ws.impl;

import com.example.srbk.facade.ProductFacade;
import com.example.srbk.model.Product;
import com.example.srbk.ws.ProductService;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@WebService(targetNamespace = "http://ws.example.com", endpointInterface = "com.example.srbk.ws.ProductService", serviceName = "ProductService")
public class ProductServiceImpl implements ProductService {

    @Inject
    private ProductFacade productFacade;

    @Override
    public List<Product> fetchAllProducts() {
        return productFacade.fetchAll();
    }

    @Override
    public List<Product> fetchAllProductsUpdatedAfterDate(Date updateTime) {
        return productFacade.fetchAllUpdatedAfterDate(updateTime);
    }

    @Override
    public void saveProduct(Product product) {
        productFacade.save(product);
    }
}
