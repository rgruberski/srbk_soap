package com.example.srbk.ws.impl;

import com.example.srbk.facade.SeanceFacade;
import com.example.srbk.model.Seance;
import com.example.srbk.ws.SeanceService;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@WebService(targetNamespace = "http://ws.example.com", endpointInterface = "com.example.srbk.ws.SeanceService", serviceName = "SeanceService")
public class SeanceServiceImpl implements SeanceService {

    @Inject
    private SeanceFacade seanceFacade;

    @Override
    public Seance getSeance(Integer id) {
        return seanceFacade.fetchOne(id);
    }

    @Override
    public Seance getSeanceWithOccupation(Integer id) {
        return seanceFacade.fetchOneWithOccupation(id);
    }

    @Override
    public List<Seance> fetchAllSeances() {
        return seanceFacade.fetchAll();
    }

    @Override
    public List<Seance> fetchAllSeancesUpdatedAfterDate(Date updateTime) {
        return seanceFacade.fetchAllUpdatedAfterDate(updateTime);
    }
}
