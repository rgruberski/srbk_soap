package com.example.srbk.ws.impl;

import com.example.srbk.facade.HallFacade;
import com.example.srbk.model.Hall;
import com.example.srbk.ws.HallService;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@WebService(targetNamespace = "http://ws.example.com", endpointInterface = "com.example.srbk.ws.HallService", serviceName = "HallService")
public class HallServiceImpl implements HallService {

    @Inject
    private HallFacade hallFacade;

    @Override
    public List<Hall> fetchAllHalls() {
        return hallFacade.fetchAll();
    }

    @Override
    public List<Hall> fetchAllHallsUpdatedAfterDate(Date updateTime) {
        return hallFacade.fetchAllUpdatedAfterDate(updateTime);
    }
}
