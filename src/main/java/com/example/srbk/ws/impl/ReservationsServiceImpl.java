package com.example.srbk.ws.impl;

import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.exception.ReservationExpiredException;
import com.example.srbk.exception.ReservationNotExistsException;
import com.example.srbk.facade.ReservationFacade;
import com.example.srbk.model.Reservation;
import com.example.srbk.ws.ReservationsService;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;

/**
 * Created by robert on 15.01.15.
 */
@WebService(targetNamespace = "http://ws.example.com", endpointInterface = "com.example.srbk.ws.ReservationsService", serviceName = "ReservationsService")
public class ReservationsServiceImpl implements ReservationsService {

    @Inject
    private ReservationFacade reservationFacade;

    @Override
    public Reservation getReservation(Integer id) throws ReservationNotExistsException, ReservationExpiredException {

        Reservation reservation = reservationFacade.getReservation(id);

        if (reservation == null) {
            throw new ReservationNotExistsException();
        } else if (reservation.getExpirationDate().getTime() < new Date().getTime()) {
            throw new ReservationExpiredException();
        }

        return reservation;
    }

    @Override
    public Reservation addPreliminaryReservation(Reservation reservation) throws ReservationBusySeatException {
        return reservationFacade.addPreliminaryReservation(reservation);
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {
        return reservationFacade.saveReservation(reservation);
    }
}
