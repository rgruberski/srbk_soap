package com.example.srbk.ws.impl;

import com.example.srbk.facade.ProductCategoryFacade;
import com.example.srbk.model.ProductCategory;
import com.example.srbk.ws.ProductCategoryService;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 13.01.15.
 */
@WebService(targetNamespace = "http://ws.example.com", endpointInterface = "com.example.srbk.ws.ProductCategoryService", serviceName = "ProductCategoryService")
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Inject
    private ProductCategoryFacade productCategoryFacade;

    @Override
    public List<ProductCategory> fetchAllProductCategories() {
        return productCategoryFacade.fetchAll();
    }

    @Override
    public List<ProductCategory> fetchAllProductCategoriesUpdatedAfterDate(Date updateTime) {
        return productCategoryFacade.fetchAllUpdatedAfterDate(updateTime);
    }
}
