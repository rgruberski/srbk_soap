package com.example.srbk.ws;

import com.example.srbk.model.Seance;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@WebService
public interface SeanceService {
    @WebMethod
    public Seance getSeance(@WebParam Integer id);

    @WebMethod
    public Seance getSeanceWithOccupation(@WebParam Integer id);

    @WebMethod
    public List<Seance> fetchAllSeances();

    @WebMethod
    public List<Seance> fetchAllSeancesUpdatedAfterDate(@WebParam Date updateTime);
}
