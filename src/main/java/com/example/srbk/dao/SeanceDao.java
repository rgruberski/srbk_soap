package com.example.srbk.dao;

import com.example.srbk.model.Seance;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
public interface SeanceDao extends GenericDao<Seance> {
    public List<Seance> findAllUpdatedAfterDate(Date updateTime);
}
