package com.example.srbk.dao;

import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
public interface GenericDao<T> {
    public T find(int id);

    public List<T> findAll();

    public void save(T entity);

    public T update(T entity);
}
