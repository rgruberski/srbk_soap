package com.example.srbk.dao;

import com.example.srbk.model.Product;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
public interface ProductDao extends GenericDao<Product> {
    public List<Product> findAllUpdatedAfterDate(Date updateTime);
}
