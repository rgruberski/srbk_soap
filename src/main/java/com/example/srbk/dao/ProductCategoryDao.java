package com.example.srbk.dao;

import com.example.srbk.model.ProductCategory;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 13.01.15.
 */
public interface ProductCategoryDao extends GenericDao<ProductCategory> {
    public List<ProductCategory> findAllUpdatedAfterDate(Date updateTime);
}
