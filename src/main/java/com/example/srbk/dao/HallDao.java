package com.example.srbk.dao;

import com.example.srbk.model.Hall;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
public interface HallDao extends GenericDao<Hall> {
    public List<Hall> findAllUpdatedAfterDate(Date updateTime);
}
