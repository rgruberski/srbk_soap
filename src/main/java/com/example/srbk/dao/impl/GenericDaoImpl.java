package com.example.srbk.dao.impl;

import com.example.srbk.dao.GenericDao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.*;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
public abstract class GenericDaoImpl<T> implements GenericDao<T> {

    private final static String UNIT_NAME = "srbkDS";

    @PersistenceContext(unitName = UNIT_NAME)
    protected EntityManager em;
    protected Class<T> entityClass;
    @Resource
    private UserTransaction utx;

    public T find(int id) {
        return em.find(entityClass, id);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<T> findAll() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return em.createQuery(cq).getResultList();
    }

    public void save(T entity) {
        try {
            utx.begin();
            em.persist(entity);
            utx.commit();
        } catch (NotSupportedException e) {
            e.printStackTrace();
        } catch (SystemException e) {
            e.printStackTrace();
        } catch (HeuristicRollbackException e) {
            e.printStackTrace();
        } catch (RollbackException e) {
            e.printStackTrace();
        } catch (HeuristicMixedException e) {
            e.printStackTrace();
        }
    }

    public T update(T entity) {
        try {
            utx.begin();
            em.merge(entity);
            utx.commit();
        } catch (NotSupportedException e) {
            e.printStackTrace();
        } catch (SystemException e) {
            e.printStackTrace();
        } catch (HeuristicRollbackException e) {
            e.printStackTrace();
        } catch (RollbackException e) {
            e.printStackTrace();
        } catch (HeuristicMixedException e) {
            e.printStackTrace();
        }

        return entity;
    }

    public void setEntityClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
}
