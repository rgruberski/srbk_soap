package com.example.srbk.dao.impl;

import com.example.srbk.dao.SeanceDao;
import com.example.srbk.model.Seance;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@Named
@RequestScoped
public class SeanceDaoImpl extends GenericDaoImpl<Seance> implements SeanceDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Seance.class);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Seance> findAllUpdatedAfterDate(Date updateTime) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Seance> c = cq.from(entityClass);
        cq.select(c);
        cq.where(cb.greaterThan(c.<Date>get("updateTime"), updateTime));
        return em.createQuery(cq).getResultList();
    }
}
