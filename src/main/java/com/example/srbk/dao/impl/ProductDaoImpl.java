package com.example.srbk.dao.impl;

import com.example.srbk.dao.ProductDao;
import com.example.srbk.model.Product;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@Named
@RequestScoped
public class ProductDaoImpl extends GenericDaoImpl<Product> implements ProductDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Product.class);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Product> findAllUpdatedAfterDate(Date updateTime) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Product> c = cq.from(entityClass);
        cq.select(c);
        cq.where(cb.greaterThan(c.<Date>get("updateTime"), updateTime));
        return em.createQuery(cq).getResultList();
    }
}
