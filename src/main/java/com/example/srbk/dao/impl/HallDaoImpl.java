package com.example.srbk.dao.impl;

import com.example.srbk.dao.HallDao;
import com.example.srbk.model.Hall;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@Named
@RequestScoped
public class HallDaoImpl extends GenericDaoImpl<Hall> implements HallDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Hall.class);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Hall> findAllUpdatedAfterDate(Date updateTime) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Hall> c = cq.from(entityClass);
        cq.select(c);
        cq.where(cb.greaterThan(c.<Date>get("updateTime"), updateTime));
        return em.createQuery(cq).getResultList();
    }
}
