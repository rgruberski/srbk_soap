package com.example.srbk.dao.impl;

import com.example.srbk.dao.ReservationDao;
import com.example.srbk.model.Reservation;
import com.example.srbk.model.Seance;
import com.example.srbk.model.Seat;
import com.example.srbk.model.Ticket;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;

/**
 * Created by robert on 14.01.15.
 */
@Named
@RequestScoped
public class ReservationDaoImpl extends GenericDaoImpl<Reservation> implements ReservationDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Reservation.class);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Reservation findOne(Seance seance, Seat seat) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Reservation> c = cq.from(entityClass);

        Join<Reservation, Ticket> tickets = c.join("tickets", JoinType.LEFT);

        cq.select(c);

        cq.where(cb.equal(c.get("seance"), seance));
        cq.where(cb.equal(tickets.get("seat"), seat));

        try {
            return (Reservation) em.createQuery(cq).getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
