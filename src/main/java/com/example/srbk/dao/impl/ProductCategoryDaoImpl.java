package com.example.srbk.dao.impl;

import com.example.srbk.dao.ProductCategoryDao;
import com.example.srbk.model.ProductCategory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 13.01.15.
 */
@Named
@RequestScoped
public class ProductCategoryDaoImpl extends GenericDaoImpl<ProductCategory> implements ProductCategoryDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(ProductCategory.class);
    }

    @Override
    public List<ProductCategory> findAllUpdatedAfterDate(Date updateTime) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<ProductCategory> c = cq.from(entityClass);
        cq.select(c);
        cq.where(cb.greaterThan(c.<Date>get("updateTime"), updateTime));
        return em.createQuery(cq).getResultList();
    }
}
