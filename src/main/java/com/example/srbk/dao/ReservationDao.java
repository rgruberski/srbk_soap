package com.example.srbk.dao;

import com.example.srbk.model.Reservation;
import com.example.srbk.model.Seance;
import com.example.srbk.model.Seat;

/**
 * Created by robert on 14.01.15.
 */
public interface ReservationDao extends GenericDao<Reservation> {
    public Reservation findOne(Seance seance, Seat seat);
}
