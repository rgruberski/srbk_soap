package com.example.srbk.enumerator;

/**
 * Created by robert on 10.01.15.
 */
public enum ProductStatus {
    AVAILABLE, UNAVAILABLE
}
