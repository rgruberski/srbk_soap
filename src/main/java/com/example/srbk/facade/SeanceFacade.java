package com.example.srbk.facade;

import com.example.srbk.model.Seance;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
public interface SeanceFacade {
    public Seance fetchOne(Integer id);

    public Seance fetchOneWithOccupation(Integer id);

    public List<Seance> fetchAll();

    public List<Seance> fetchAllUpdatedAfterDate(Date updateTime);
}
