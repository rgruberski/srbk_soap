package com.example.srbk.facade.impl;

import com.example.srbk.dao.ReservationDao;
import com.example.srbk.dao.SeanceDao;
import com.example.srbk.facade.SeanceFacade;
import com.example.srbk.model.Reservation;
import com.example.srbk.model.Seance;
import com.example.srbk.model.Seat;
import com.example.srbk.model.SeatsRow;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@Named
@RequestScoped
public class SeanceFacadeImpl implements SeanceFacade {

    @Inject
    private SeanceDao seanceDao;

    @Inject
    private ReservationDao reservationDao;

    @Override
    public Seance fetchOne(Integer id) {
        return seanceDao.find(id);
    }

    @Override
    public Seance fetchOneWithOccupation(Integer id) {

        Seance seance = seanceDao.find(id);

        for (SeatsRow seatsRow : seance.getHall().getSeatsRows()) {

            for (Seat seat : seatsRow.getSeatList()) {

                Reservation reservation = reservationDao.findOne(seance, seat);

                if (reservation != null) {
                    seat.setOccupied(true);
                }
            }
        }

        seance.getHall().setSeatsRowsWithOccupation(seance.getHall().getSeatsRows());

        return seance;

    }

    @Override
    public List<Seance> fetchAll() {
        return seanceDao.findAll();
    }

    @Override
    public List<Seance> fetchAllUpdatedAfterDate(Date updateTime) {
        return seanceDao.findAllUpdatedAfterDate(updateTime);
    }
}
