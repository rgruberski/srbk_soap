package com.example.srbk.facade.impl;

import com.example.srbk.dao.ProductDao;
import com.example.srbk.facade.ProductFacade;
import com.example.srbk.model.Product;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@Named
@RequestScoped
public class ProductFacadeImpl implements ProductFacade {

    @Inject
    private ProductDao productDao;

    @Override
    public List<Product> fetchAll() {
        return productDao.findAll();
    }

    @Override
    public List<Product> fetchAllUpdatedAfterDate(Date updateTime) {
        return productDao.findAllUpdatedAfterDate(updateTime);
    }

    @Override
    public void save(Product product) {
        productDao.save(product);
    }
}
