package com.example.srbk.facade.impl;

import com.example.srbk.dao.ReservationDao;
import com.example.srbk.dao.SeanceDao;
import com.example.srbk.enumerator.ReservationStatus;
import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.facade.ReservationFacade;
import com.example.srbk.model.Reservation;
import com.example.srbk.model.Seance;
import com.example.srbk.model.Seat;
import com.example.srbk.model.Ticket;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

/**
 * Created by robert on 15.01.15.
 */
@Named
@RequestScoped
public class ReservationFacadeImpl implements ReservationFacade {

    @Inject
    private ReservationDao reservationDao;

    @Inject
    private SeanceDao seanceDao;

    @Override
    public Reservation getReservation(Integer id) {

        Reservation reservation = reservationDao.find(id);

        for (Ticket ticket : reservation.getTickets()) {
            ticket.getSeat().setRow(ticket.getSeat().getSeatsRow().getSymbol());
        }

        return reservation;
    }

    @Override
    public Reservation addPreliminaryReservation(Reservation reservation) throws ReservationBusySeatException {

        Seance seance = seanceDao.find(reservation.getSeance().getId());

        for (Ticket ticket : reservation.getTickets()) {
            ticket.setReservation(reservation);

            Reservation existedReservation = reservationDao.findOne(seance, new Seat(ticket.getSeat().getId()));

            if (existedReservation != null) {
                throw new ReservationBusySeatException();
            }
        }

        reservation.setExpirationDate(new Date(new Date().getTime() + (1000 * 60 * 15)));

        reservationDao.save(reservation);
        return reservation;
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {

        for (Ticket ticket : reservation.getTickets()) {
            ticket.setReservation(reservation);
        }

        if (reservation.getStatus() == ReservationStatus.CONFIRMED) {
            Date seanceStartTime = seanceDao.find(reservation.getSeance().getId()).getStartTime();
            reservation.setExpirationDate(new Date(seanceStartTime.getTime() - (1000 * 60 * 15)));
        }

        reservationDao.update(reservation);
        return reservation;
    }
}
