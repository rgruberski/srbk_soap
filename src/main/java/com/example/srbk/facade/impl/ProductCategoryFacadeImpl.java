package com.example.srbk.facade.impl;

import com.example.srbk.dao.ProductCategoryDao;
import com.example.srbk.facade.ProductCategoryFacade;
import com.example.srbk.model.ProductCategory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 13.01.15.
 */
@Named
@RequestScoped
public class ProductCategoryFacadeImpl implements ProductCategoryFacade {

    @Inject
    private ProductCategoryDao productCategoryDao;

    @Override
    public List<ProductCategory> fetchAll() {
        return productCategoryDao.findAll();
    }

    @Override
    public List<ProductCategory> fetchAllUpdatedAfterDate(Date updateTime) {
        return productCategoryDao.findAllUpdatedAfterDate(updateTime);
    }
}
