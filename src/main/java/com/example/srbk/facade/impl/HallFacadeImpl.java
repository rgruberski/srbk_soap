package com.example.srbk.facade.impl;

import com.example.srbk.dao.HallDao;
import com.example.srbk.facade.HallFacade;
import com.example.srbk.model.Hall;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@Named
@RequestScoped
public class HallFacadeImpl implements HallFacade {

    @Inject
    private HallDao hallDao;

    @Override
    public List<Hall> fetchAll() {
        return hallDao.findAll();
    }

    @Override
    public List<Hall> fetchAllUpdatedAfterDate(Date updateTime) {
        return hallDao.findAllUpdatedAfterDate(updateTime);
    }
}
