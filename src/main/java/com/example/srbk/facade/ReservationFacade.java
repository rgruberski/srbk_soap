package com.example.srbk.facade;

import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.model.Reservation;

/**
 * Created by robert on 15.01.15.
 */
public interface ReservationFacade {
    public Reservation getReservation(Integer id);

    public Reservation addPreliminaryReservation(Reservation reservation) throws ReservationBusySeatException;

    public Reservation saveReservation(Reservation reservation);
}
