package com.example.srbk.facade;

import com.example.srbk.model.ProductCategory;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 13.01.15.
 */
public interface ProductCategoryFacade {
    public List<ProductCategory> fetchAll();

    public List<ProductCategory> fetchAllUpdatedAfterDate(Date updateTime);
}
