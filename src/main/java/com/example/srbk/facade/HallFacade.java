package com.example.srbk.facade;

import com.example.srbk.model.Hall;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
public interface HallFacade {
    public List<Hall> fetchAll();

    public List<Hall> fetchAllUpdatedAfterDate(Date updateTime);
}
