package com.example.srbk.facade;

import com.example.srbk.model.Product;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
public interface ProductFacade {
    public List<Product> fetchAll();

    public List<Product> fetchAllUpdatedAfterDate(Date updateTime);

    public void save(Product product);
}
