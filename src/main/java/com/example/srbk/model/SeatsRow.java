package com.example.srbk.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "seats_row")
public class SeatsRow {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "hall_id")
    private Hall hall;

    @OneToMany(mappedBy = "seatsRow", fetch = FetchType.EAGER)
    private List<Seat> seatList;

    private String symbol;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Seat> seatList) {
        this.seatList = seatList;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
