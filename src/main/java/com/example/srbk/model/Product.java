package com.example.srbk.model;

import com.example.srbk.enumerator.ProductStatus;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "products")
@XmlRootElement
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Product implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private ProductCategory category;

    private ProductStatus status;

    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    private String picture;

    private Integer ageRestriction;

    @XmlTransient
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    private List<Seance> seances;

    @Column(nullable = false)
    //@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private Date updateTime;

    @PrePersist
    protected void onCreate() {
        updateTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateTime = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getAgeRestriction() {
        return ageRestriction;
    }

    public void setAgeRestriction(Integer ageRestriction) {
        this.ageRestriction = ageRestriction;
    }

    public List<Seance> getSeances() {
        return seances;
    }

    public void setSeances(List<Seance> seances) {
        this.seances = seances;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
