package com.example.srbk.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "seats")
@XmlRootElement
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Seat implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "seats_row_id")
    @XmlTransient
    private SeatsRow seatsRow;

    private Integer number;

    @Transient
    private String row;

    @Transient
    private boolean occupied;

    public Seat() {

    }

    public Seat(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SeatsRow getSeatsRow() {
        return seatsRow;
    }

    public void setSeatsRow(SeatsRow seatsRow) {
        this.seatsRow = seatsRow;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
}
