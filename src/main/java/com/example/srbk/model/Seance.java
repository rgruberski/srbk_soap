package com.example.srbk.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "seances")
public class Seance implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "hall_id")
    private Hall hall;

    @Column
    //@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private Date startTime;

    @Column(nullable = false)
    //@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private Date updateTime;

    @PrePersist
    protected void onCreate() {
        updateTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateTime = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
