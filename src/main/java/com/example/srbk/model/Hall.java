package com.example.srbk.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "halls")
@XmlRootElement
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Hall implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @XmlTransient
    @OneToMany(mappedBy = "hall", fetch = FetchType.EAGER)
    private List<SeatsRow> seatsRows;

    @Transient
    private List<SeatsRow> seatsRowsWithOccupation;

    private String name;

    @Column(nullable = false)
    //@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private Date updateTime;

    @PrePersist
    protected void onCreate() {
        updateTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateTime = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<SeatsRow> getSeatsRows() {
        return seatsRows;
    }

    public void setSeatsRows(List<SeatsRow> seatsRows) {
        this.seatsRows = seatsRows;
    }

    public List<SeatsRow> getSeatsRowsWithOccupation() {
        return seatsRowsWithOccupation;
    }

    public void setSeatsRowsWithOccupation(List<SeatsRow> seatsRowsWithOccupation) {
        this.seatsRowsWithOccupation = seatsRowsWithOccupation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
