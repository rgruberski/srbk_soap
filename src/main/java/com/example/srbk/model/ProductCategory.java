package com.example.srbk.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "product_categories")
public class ProductCategory implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @Column(nullable = false)
    //@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private Date updateTime;

    @PrePersist
    protected void onCreate() {
        updateTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateTime = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
